package com.spider.jsoup;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class MyJsoup {

	public static void main(String[] args) throws Exception{
		
		Document doc = Jsoup.connect("http://www.itcast.cn").get();
		
		Elements tag = doc.getElementsByTag("a");
		
		System.out.println(tag.text());
	}
}
