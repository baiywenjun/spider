package com.spider.http;

import java.io.IOException;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.util.EntityUtils;

public class MyHttpClient {

	public static void main(String[] args) throws Exception {
		
		HttpClient client = new DefaultHttpClient();
		
//	client.getParams()
//		.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 10000)
//		.setParameter(CoreConnectionPNames.SO_TIMEOUT, 10000)
//		.setParameter(ConnRouteParams.DEFAULT_PROXY, new HttpHost("218.17.30.29",8888));
		
		HttpGet get = new HttpGet("http://www.itcast.cn/");
		HttpResponse response = client.execute(get);
		
		String content = EntityUtils.toString(response.getEntity(), "utf-8");
		
		System.out.println(content);
		
		
	}
}
