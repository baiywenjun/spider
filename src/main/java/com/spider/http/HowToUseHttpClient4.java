package com.spider.http;

import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class HowToUseHttpClient4 {

	public static void main(String[] args) throws Exception {
		
		String url = "https://www.cnblogs.com";
		
		CloseableHttpClient client = HttpClients.createDefault();
		HttpGet get = new HttpGet(url);
		
		CloseableHttpResponse response = client.execute(get);
			
		StatusLine statusLine = response.getStatusLine();	
		System.out.println(statusLine);
		
		HttpEntity entity = response.getEntity();
		System.out.println(EntityUtils.toString(entity));
		
		EntityUtils.consume(entity);		
	}
}
